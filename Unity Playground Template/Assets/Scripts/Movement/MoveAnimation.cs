﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAnimation : MonoBehaviour
{
    [Header("Animation")]
    public Enums.MovementType movementType = Enums.MovementType.AllDirections;
    public Animator objectToAnimate = null;
    public bool flipMoveRight = false;
    public bool flipMoveLeft = false;
    public bool useFallAnimation = false;

    private Rigidbody2D rb = null;
    private bool touchingGround = false;

    // Start is called before the first frame update
    void Start()
    {
        // If no specific object was requested to be animated, assume it either on the current object
        if (objectToAnimate == null)
        {
            objectToAnimate = GetComponent<Animator>();
        }

        if (objectToAnimate == null)
        {
            Debug.LogError("Failed to set up movement animation - no animator provided and no animator found on object " + name);
        }
        else
        {
            rb = objectToAnimate.GetComponent<Rigidbody2D>();

            if (rb == null)
            {
                Debug.LogError("Failed to set up movement animation - no rigidbody found on object " + name);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Animation
        if (rb.velocity.x != 0 && (movementType == Enums.MovementType.AllDirections 
            || movementType == Enums.MovementType.OnlyHorizontal))
        {
            objectToAnimate.SetFloat("horizontal", rb.velocity.x);

            if (flipMoveLeft)
                objectToAnimate.GetComponent<SpriteRenderer>().flipX = rb.velocity.x < 0;
            else if (flipMoveRight)
                objectToAnimate.GetComponent<SpriteRenderer>().flipX = rb.velocity.x > 0;
        }
        if (rb.velocity.y != 0 && (movementType == Enums.MovementType.AllDirections
            || movementType == Enums.MovementType.OnlyVertical))
        {
            objectToAnimate.SetFloat("vertical", rb.velocity.y);
        }


        objectToAnimate.SetFloat("speed", rb.velocity.magnitude);

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Ground"))
        {
            touchingGround = true;
        }
    }

    private void LateUpdate()
    {
        if (useFallAnimation)
            objectToAnimate.SetBool("falling", !touchingGround);

        touchingGround = false;
    }
}
